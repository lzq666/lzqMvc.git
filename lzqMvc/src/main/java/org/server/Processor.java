package org.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Processor extends Thread {
	
	private Socket socket;
	private InputStream in;
	private PrintStream out;
	private final static String ROOT = "D:\\server";//服务器可以访问的路径
	public Processor(Socket socket){
		this.socket=socket;
		try {
			in = socket.getInputStream();
			out = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		String fileName = parse(in);
		sendFile(fileName);
	}
	
	public String parse(InputStream in) {
		BufferedReader bReader = new BufferedReader(new InputStreamReader(in));
		String fileName = null;
		try {
			String httpMessage = bReader.readLine();
			String [] content = httpMessage.split(" ");
			if(content.length!=3){
				sendErrorMessAge(400,"请求错误");
			}
			fileName = content[1];
			System.out.println("code="+content[0]+"||filename="+content[1]+"||http version="+content[2]);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileName;
	}
	//输出错误信息
	public void sendErrorMessAge(int errorCode,String errorMessage) {
		out.println("http/1.0 "+errorCode+" "+errorMessage);
		out.println("content-type: text/html;");
		out.println();
		out.println("<html>");
		out.println("<title>Error Message</title>");
		out.println("<body>");
		out.println("<h1>ErrorCode:"+errorCode+",ErrorMessage"+errorMessage+"</h1>");
		out.println("</body>");
		out.println("</html>");
		out.flush();
		out.close();
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//输出访问资源
	public void sendFile(String fileName) {
		File file = new File(Processor.ROOT+fileName);
		if (!file.exists()) {
			sendErrorMessAge(404, "not found");
			return;
		}
		try {
			InputStream in = new FileInputStream(file);
			byte [] content = new byte[(int)file.length()];
			in.read(content);
			out.println("http/1.0 200 queryfile");
			out.println("content-lenght:"+content.length);
			out.println();
			out.write(content);
			out.flush();
			out.close();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
