package org.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	//监听端口号
	public void startServer(int port) {
		try {
			ServerSocket serverSocket = new ServerSocket(port);
			while (true) {//开启一个永不关闭的循环，等待端口访问
				Socket socket = serverSocket.accept();
				new Processor(socket).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Server().startServer(8090);
	}
}
